package main

import (
	"database/sql"
	"log"
)

type Store interface {
	CreateProfile(profile *Profile) error
	CreateHNProfile(hnProfile *HNProfile) error
	CreateHNItem(hnItem *HNItem) error
}

type dbStore struct {
	db *sql.DB
}

func (store *dbStore) CreateProfile(profile *Profile) error {
	_, err := store.db.Query("INSERT INTO profiles(hn_username) VALUES ($1)", profile.HNUsername)
	return err
}

func (store *dbStore) CreateHNProfile(hnProfile *HNProfile) error {
	instmt := "INSERT INTO hn_profiles(id, delay, created, karma, about) VALUES ($1, $2, $3, $4, $5)"

	stmt, err := store.db.Prepare(instmt)
	if err != nil {
		log.Fatal(err)
		return err
	}

	res, err := stmt.Exec(
		hnProfile.Id,
		hnProfile.Delay,
		hnProfile.Created,
		hnProfile.Karma,
		hnProfile.About,
	)
	if err != nil || res == nil {
		log.Fatal(err)
		return err

	}

	stmt.Close()
	return nil
}

func (store *dbStore) CreateHNItem(hnItem *HNItem) error {

	instmt := "INSERT INTO hn_items(id, deleted, type, by, time, text, dead, parent, poll, url, score, title, descendents) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)"

	stmt, err := store.db.Prepare(instmt)
	if err != nil {
		log.Fatal(err)
		return err
	}

	res, err := stmt.Exec(
		hnItem.Id,
		hnItem.Deleted,
		hnItem.Type,
		hnItem.By,
		hnItem.Time,
		hnItem.Text,
		hnItem.Dead,
		hnItem.Parent,
		hnItem.Poll,
		hnItem.Url,
		hnItem.Score,
		hnItem.Title,
		hnItem.Descendents,
	)
	if err != nil || res == nil {
		log.Fatal(err)
		return err

	}

	stmt.Close()
	return nil
}

var store Store

func InitStore(s Store) {
	store = s
}
