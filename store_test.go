package main

import (
	"database/sql"
	"testing"

	_ "github.com/lib/pq"
	"github.com/stretchr/testify/suite"
)

type StoreSuite struct {
	suite.Suite

	store *dbStore
	db    *sql.DB
}

func (s *StoreSuite) SetupSuite() {
	connString := "dbname=theprofileapp sslmode=disable"
	db, err := sql.Open("postgres", connString)
	if err != nil {
		s.T().Fatal(err)
	}
	s.db = db
	s.store = &dbStore{db: db}
}

func (s *StoreSuite) SetupTest() {
	_, err := s.db.Query("DELETE from profiles")
	if err != nil {
		s.T().Fatal(err)
	}
	_, err = s.db.Query("DELETE from hn_profiles")
	if err != nil {
		s.T().Fatal(err)
	}
	_, err = s.db.Query("DELETE from hn_items")
	if err != nil {
		s.T().Fatal(err)
	}

}

func (s *StoreSuite) TearDownSuite() {
	s.db.Close()
}

func TestStoreSuite(t *testing.T) {
	s := new(StoreSuite)
	suite.Run(t, s)
}

func (s *StoreSuite) TestCreatingProfile() {
	s.store.CreateProfile(&Profile{
		HNUsername: "kgthegreat",
	})

	res, err := s.db.Query(`SELECT COUNT(*) FROM profiles WHERE hn_username='kgthegreat'`)
	if err != nil {
		s.T().Fatal(err)
	}

	var count int
	for res.Next() {
		err := res.Scan(&count)
		if err != nil {
			s.T().Error(err)
		}
	}

	if count != 1 {
		s.T().Errorf("incorrect count, wanted 1 got %d", count)
	}
}

func (s *StoreSuite) TestCreatingHNProfile() {
	s.store.CreateHNProfile(&HNProfile{
		Id:      "random",
		Delay:   0,
		Created: 1173923446,
		Karma:   2937,
		About:   "A test",
	})

	res, err := s.db.Query(`SELECT COUNT(*) FROM hn_profiles WHERE id='random'`)
	if err != nil {
		s.T().Fatal(err)
	}

	var count int
	for res.Next() {
		err := res.Scan(&count)
		if err != nil {
			s.T().Error(err)
		}
	}

	if count != 1 {
		s.T().Errorf("incorrect count, wanted 1 got %d", count)
	}
}

func (s *StoreSuite) TestCreatingHNItem() {
	s.store.CreateHNItem(&HNItem{
		Id:          126809,
		Deleted:     false,
		Type:        "story",
		By:          "random",
		Time:        1175714200,
		Text:        "This is test",
		Dead:        false,
		Parent:      123456,
		Poll:        234567,
		Url:         "http://google.com",
		Score:       111,
		Title:       "Test Title",
		Descendents: 5,
	})

	res, err := s.db.Query(`SELECT COUNT(*) FROM hn_items WHERE id=126809`)
	if err != nil {
		s.T().Fatal(err)
	}

	var count int
	for res.Next() {
		err := res.Scan(&count)
		if err != nil {
			s.T().Error(err)
		}
	}

	if count != 1 {
		s.T().Errorf("incorrect count, wanted 1 got %d", count)
	}
}
