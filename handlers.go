package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

func hnHandler(w http.ResponseWriter, r *http.Request) {
	profile := Profile{}
	err := r.ParseForm()

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	uname := r.Form.Get("hnusername")
	profile.HNUsername = uname
	err = store.CreateProfile(&profile)

	if err != nil {
		fmt.Println(err)
	}

	c := make(chan HNProfile)
	//	fmt.Fprintln(w, "Profile Created")
	go getHn(uname, c)

	hnProfile := <-c

	//	fmt.Fprintln(w, hnProfile.Id)

	renderTemplate(w, "profile",
		map[string]string{
			"Id":    hnProfile.Id,
			"About": hnProfile.About,
			"Karma": hnProfile.Karma,
		})

}

func getHn(uname string, c chan HNProfile) {

	var url string = "https://hacker-news.firebaseio.com/v0/user/" + uname + ".json"
	var ch chan HTTPResponse = make(chan HTTPResponse)
	go DoHTTPGet(url, ch)
	hnProfile := HNProfile{}
	jsonErr := json.Unmarshal((<-ch).body, &hnProfile)
	if jsonErr != nil {
		log.Fatal(jsonErr)

	}
	err := store.CreateHNProfile(&hnProfile)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(len(hnProfile.Submitted))
	for _, element := range hnProfile.Submitted {

		var url string = "https://hacker-news.firebaseio.com/v0/item/" + strconv.Itoa(element) + ".json"
		go DoHTTPGet(url, ch)
	}
	for range hnProfile.Submitted {
		hnItem := HNItem{}
		jsonErr := json.Unmarshal((<-ch).body, &hnItem)
		if jsonErr != nil {
			log.Fatal(jsonErr)

		}

		err := store.CreateHNItem(&hnItem)

		if err != nil {
			fmt.Println(err)
		}
		//		store.Close()

		fmt.Println(hnItem.Text)
	}

	c <- hnProfile

}

func indexHandler(w http.ResponseWriter, req *http.Request) {

	var empty []string
	renderTemplate(w, "index", empty)

}

func DoHTTPGet(url string, ch chan<- HTTPResponse) {
	httpResponse, err := http.Get(url)
	if err != nil {
		//httpResponse.Body.Close()
		fmt.Println(err)
		return
	}
	httpBody, err := ioutil.ReadAll(httpResponse.Body)

	if err != nil {
		fmt.Println(err)
	} else {
		ch <- HTTPResponse{httpResponse.Status, httpBody}
	}

}
