package main

type HTTPResponse struct {
	status string
	body   []byte
}

type Profile struct {
	HNUsername string `json:"hnUsername"`
}

type HNProfile struct {
	About     string `json:"about"`
	Created   int    `json:"created"`
	Delay     int    `json:"delay"`
	Id        string `json:"id"`
	Karma     int    `json:"karma"`
	Submitted []int  `json:"submitted"`
}

type HNItem struct {
	Id          int    `json:"id"`
	Deleted     bool   `json:"created"`
	Type        string `json:"type"`
	By          string `json:"by"`
	Time        int    `json:"time"`
	Text        string `json:"text"`
	Dead        bool   `json:"dead"`
	Parent      int    `json:"parent"`
	Poll        int    `json:"poll"`
	Kids        []int  `json:"kids"`
	Url         string `json:"url"`
	Score       int    `json:"score"`
	Title       string `json:"title"`
	Parts       []int  `json:"parts"`
	Descendents int    `json:"descendents"`
}
