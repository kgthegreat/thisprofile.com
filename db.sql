CREATE TABLE hn_profiles (
       id varchar(256) PRIMARY KEY,
       about text,
       created int,
       delay int,
       karma int
       );

CREATE TABLE hn_items (
       id bigint PRIMARY KEY,
       deleted boolean,
       type varchar(256),
       by varchar(256),
       time bigint,
       text text,
       dead boolean,
       parent bigint,
       poll bigint,
       url text,
       score int,
       title text,
       descendents int
       );
