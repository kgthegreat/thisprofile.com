package main

import (
	"database/sql"
	"flag"
	"log"
	"net/http"

	_ "github.com/lib/pq"

	"github.com/gorilla/mux"
)

func main() {

	var (
		addr string = ":4000"
	)

	connString := "dbname=theprofileapp sslmode=disable"
	db, err := sql.Open("postgres", connString)
	if err != nil {
		panic(err)

	}
	err = db.Ping()

	if err != nil {
		panic(err)

	}

	InitStore(&dbStore{db: db})
	flag.StringVar(&addr, "addr", ":4000", "")
	flag.Parse()
	server := NewServer(addr)
	StartServer(server)
}

func NewServer(addr string) *http.Server {
	// Setup router
	//	initRouting()
	initStaticRouting()

	r := mux.NewRouter()
	http.Handle("/", r)
	r.HandleFunc("/", indexHandler)
	r.HandleFunc("/hn", hnHandler)

	// Create and start server
	return &http.Server{
		Addr: addr,
	}
}

func StartServer(server *http.Server) {
	log.Println("Starting server and listening on the port", server.Addr)
	err := server.ListenAndServe()
	if err != nil {
		log.Fatalln("Error: %v", err)
	}
}
